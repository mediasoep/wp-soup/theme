<?php

return [
    'primary' => '#245266',
    'secondary' => '#2d8bb3',
    'success' => '#3adb76',
    'warning' => '#ffae00',
    'alert' => '#cc4b37',
    'cta' => '#f28b15',
    'info' => '#17a2b8',
    'light-gray' => '#f1f5f9',
    'medium-gray' => '#5e626e',
    'dark-gray' => '#424242',
    'black' => '#0d2833',
    'white' => '#ffffff',
];
